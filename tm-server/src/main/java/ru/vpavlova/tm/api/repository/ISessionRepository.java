package ru.vpavlova.tm.api.repository;

import ru.vpavlova.tm.api.IRepository;
import ru.vpavlova.tm.entity.Session;

public interface ISessionRepository extends IRepository<Session> {

}
